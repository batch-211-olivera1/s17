console.log("Hello World");

// Functions
	// Lines/blocks of code that tell our device/application to perform a certain task when called/invoked
	// Mostly created to create complicated tasks to run several lines of code in succession
	// They are also used to prevent repeating lines/blocks of codes that perform the same task/function

	// Function Declarations

		// (function statement) defines a function with the specified parameters

		/*
			Syntax:

			function functionName() {
				code block (statement)
			}

		*/
		// function keyword - used to define a javascript function
		// functionName - the function name. Functions are named to be able to use later in the code.
		// function block ({}) - the statements which comprise the body of the function. this is where the code is to be executed.
		// we can assign a variable to hold a function, but that will explained later

		 function printName() {
			console.log("My name is John");

		}

		printName();

		// Semicolons are used to separate executable JS statements

		// Function Invocation
			// The code block and statements inside a function is not immediately executed when the function is defined. The code block and statements inside a function is executed when the function is invoked or called
			// it is common to use the term "call a function" instead of "invoke a function"

				printName();

				// declaredFunction(); // error // much like our variables, we cannot invoke a function we have yet to define

			// Function Declarations vs Expressions

				// Function Declarations
					// can be created through function declaration by using the function keyword and adding a function name
					// declared functions are not executed immediately. they are "saved for later use", and will be executed later, when they are invoked (call upon)


					function declaredFunction() //declared functions can be hoisted, as long as the function has been defined
					{
						console.log("Hello World from declaredFunction()")
					}

					//note: hoisting is JS's behavior for certain variables and funtions to run or use them before their declaration.

						declaredFunction();
					

					// Function Expression
						// a function can also be stored in a "variable". called function expression
						// a function expression is an anonymous function - is assigned to the variableFunction


						// Anonymous function - function w/o a name

						// variableFunction() // error function expressions, being stored in a let or const variable cannot be hoisted

						let variableFunction = function() {
							console.log("Hello Again");
						}
						variableFunction()

						// We can also create a function expression of a named function
						// However, to invoke the function expression, we invoke it by its variable name, not by its function name
						// Function expressions are always invoked "called" using the variable

						let funcExpression = function funcName() {
							console.log("Hello from the other side");
						}

						// funcName(); 
						funcExpression()

						// You can reassign declared functions and function expressions to new anonymous functions
						declaredFunction = function() {
							console.log("update declaredFunction")
						}

						declaredFunction();

						funcExpression = function() {
							console.log("updated funcExpression");
						}
						funcExpression();

						// We cannot re-assign a function expression initialized with const
						const constantFunc = function() {

							console.log("Initialized with const!")
						}
						constantFunc();

						// constantFunc = function() {
						// 	console.log("Cannot be re-assigned!")
						// }
						// constantFunc(); //error
	
				// Function Scoping
				/*
					Scope is the accessibility (visibility) of variables within our program
					JS Variables has 3 types of scope:
					1. local/block scope - within the curly braces
					2. global scope
					3. function scope
				*/

				// let globalVar = "Mr. Worlwide";
				// console.log(globalVar);
				{

					let localVar = "Armando Perez";
					console.log(localVar);
					// console.log(globalVar); //error
					// we cannot invoke a global var inside a block if it is not invoked before our code block
				}

					let globalVar = "Mr. Worlwide";
					console.log(globalVar);

				// console.log(localVar); //error
				// localVar being inside a block, cannot be accessed outside of its code block

				// Function Scope

				/*
					JS has a function scope: Each function creates a new scope variables defined inside a function are not accessible (visible) from outside the function
					Variables declared with var, let and const are quite similar when declared inside a function
				*/

				function showNames() {
					// Function Scoped variables
				var functionVar = "Joe";
				const functionConst = "John";
				let functionLet = "Jane";

				console.log(functionVar);
				console.log(functionConst);
				console.log(functionLet);

				}

				showNames();

				// console.log(functionVar);
				// console.log(functionConst);
				// console.log(functionLet);

				/*the variables, functionVar, functionConst, functionLet are function scoped and cannot be accessed outside the function they were declarred in

				*/

				// Nested Function
					// You can create another function inside a function
					// this is called a nested function

					function myNewFunction() {
						let name = "Cee";

						function nestedFunction() {
							let nestedName = "Thor";
							console.log(name);
							console.log(nestedName);
						}
						// console.log(nestedName);//error
						// nestedName is not defined
						// nestedName variable, being declared in the nestedFunction cannot be accessed outside of the function it was declared in
						nestedFunction();
					}
					myNewFunction();

					// nestedFunction();//error
					/*
					this function is declared inside myNewFunction, it cannot be invoked outside the function it was declared in

					*/

				// Function and Global scope Variables

				let globalName = "Nej";

				function myNewFunction2() {
					let nameInside = "Martin";
					// Variables declared globally(outside any function) have global scope
					// Global variable can be accessed from anywhere in a JS program including from a function
					console.log(globalName);
				}
				myNewFunction2();


		// Alert
			// alert allows us to show a small windows at the top of our browser page to show information to our users
			// as opposed to a console.log() which will only show the message on the console
			// it allows us to show short dialog or instruction to our users
			// the page will wait until the user dismisses the dialog


			alert("Hello World");// this will run immediately when the page loads
			// alert() syntax
			// alert("<messageInString>");
			// you can do it numbers too

			// can also use an alert() to show a message to user from a later function invocation

			function showSampleAlert() {
				alert("Hello, User!")
			}
			showSampleAlert();
			

			// you will find at the page wasits for the user to dismiss the dialog before proceeding
			// you can witness this by reloading the page while the console is open
			console.log("I will only log in the console when the alert is dismissed");

			// Notes on the used of alert():
				// Show only an alert() for short dialogs/messages to the user
				// do not overuse alert() because the program/js has to wait for it to be dismissed before continuing

			// Using prompt()
				// allows us to show a small window at the top of our browser to gather user input
				// it, much like alert(), will have the page waits until the user completes or enters their input
				// the input from the prompt() will be returned as a string once the user dismisses the window

				let samplePrompt = prompt("Enter your name");
				console.log("Hello," + samplePrompt);
				// console.log concatinating - ("Hello," + samplePrompt);

				/*
				prompt() syntax:

				promt("<dialogInString>");

				*/

				let sampleNullPrompt = prompt("Don't Enter anything");
				console.log(sampleNullPrompt);
				// promt() returns an empty string when there is no input
				// null if the user cancels the prompt();

				// prompt() can be used for us to gather user input and be used in our code
				// however, since prompt() windows will have the page wait the user dismisses the window it must not overused

				// prompt() used globally will be run immediately so, for better user experience, it is much better to use them accordingly or add them in a function

				function printWelcomeMessage(){
					let firstName = prompt("Enter your name")
					let lastName = prompt("Enter your last name");
					console.log("Hello " + firstName + " " + lastName + "!");
					console.log("Welcome to the page");

						}

					printWelcomeMessage();

					// Function Naming Conventions

					// function names should be definitive of the task it will perform. it usually contains a verb

					function getCourses() {
						let courses = ["Science 101", "Math 101", "English 101"];
						console.log(courses);
						}
						getCourses();

					// Avoid generic names to avoid confusion within your code

					function get() {
						let name = "Jamie";
						console.log(name);
					}

					get();

					// Avoid pointless and inappropriate function names

					function pikachu() {
							console.log(25%25)

					}
					pikachu();

					// Name your functions in small caps. follow camelCase when naming variables and functions

					function displayCarInfo() {
						console.log("Brand: Toyota");
						console.log("Type: Sedan");
						console.log("Price: 12312312312");
					}

					displayCarInfo();
