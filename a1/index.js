console.log("Hello World")


/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function localInfo() {

		let info1 = prompt("Please enter your Fullname");
		let info2 = prompt("How old are you?");
		let info3 = prompt("Where are you located?");
		console.log("Hello " + info1);
		console.log("Your are " + info2 + " years old");
		console.log("You live in " + info3);
		alert("Thank you");
	}
	
	localInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function printBands() {
		console.log("My Top 5 Bands:");
		console.log("1. The Beatles");
		console.log("2. Eagles");
		console.log("3. Queen");
		console.log("4. Led Zeppelin");
		console.log("5. Pink Floyd");
	}
		printBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function favMovies() {
		console.log("1. THE MATRIX RESURRECTIONS");
		console.log("Rotten Tomatoes Rating: 63%")
		console.log("2. Avengers: Endgame");
		console.log("Rotten Tomatoes Rating: 94%")
		console.log("3. Saving Private Ryan");
		console.log("Rotten Tomatoes Rating: 94%")
		console.log("4. Hacksaw Ridge");
		console.log("Rotten Tomatoes Rating: 84%")
		console.log("5. Fury");
		console.log("Rotten Tomatoes Rating: 76%")
	}	
		favMovies();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers()
{
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
	
}

printFriends();
	

// console.log(friend1);
// console.log(friend2);
